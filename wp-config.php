<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'msn_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'oUF+ax@oEX<NS2-dtZZu]|V}9Ro)FTS8]:r&I>ECB{A#FFpcUT|spH4;O,_L_rxO');
define('SECURE_AUTH_KEY',  '82^ix[Y/m_IgBx@K-P yWu#HLySiT9=iEFLYt5mH|}>fj2HG{A;~zih#X]=UfGq[');
define('LOGGED_IN_KEY',    '$HKV6AV4bjCw${E{r{4v|_JTFYq]frA_cd~duC0,){;y*T^{Xw9R{G.}=aU(Wb a');
define('NONCE_KEY',        'xNgn+BT1pyP3/>ERDu Oe*Ee:]lw.=a 8O{v)kt%z*ZXvk,5|kQhcZ#$X)vG}5L7');
define('AUTH_SALT',        'rVW>D2SU[b>.7_=`o[vzQKF.kh##k]TVKS^*MD/5&S: |8V)XN_58<gTF4u2M~Cu');
define('SECURE_AUTH_SALT', '2y>$?qF9G%VK,MB#f&h%>yG}H<Xk59s8D/?~jw5Y{fe>&CWUz6iUA@pssB KeQGZ');
define('LOGGED_IN_SALT',   '/0<2k0I:1OX0#=r3^r2 i;fw.Ha}UERzP=?RT&!4c*GD1PwWdf1!}R*Ldg:yH=yZ');
define('NONCE_SALT',       'nb9mLr2nX#1>BT=JQ?E*u*ma/I}09L:ZeI4:mYBJ.xG(a|^B:g{OiK<j%Jz$AF%F');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'msn_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
