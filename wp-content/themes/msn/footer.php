<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

		</section>

		<footer class="footer">

			<div class="footer-top">
				<div class="row expanded five-padding align-middle">
					<div class="column large-7 medium-12 small-12">
						<?php wp_nav_menu( array('menu' => 'Footer Menu') ); ?>
					</div>
					<div class="column large-5 medium-12 small-12 text-right">
						<ul class="footer-social-icons">
							<li><a target="_blank" href="<?php the_field('facebook','option'); ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a target="_blank" href="<?php the_field('twitter','option'); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a target="_blank" href="<?php the_field('instagram','option'); ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>

			<p class="footer-copy text-center">My Social Network Limited.  <span>|</span>  Company Reg No 00000000  <span>|</span>  Designed by <a href="https://sixthstory.co.uk/" target="_blank">Sixth Story</a></p>
		</footer>

		<?php do_action( 'foundationpress_layout_end' ); ?>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
		</div><!-- Close off-canvas content -->
	</div><!-- Close off-canvas wrapper -->
<?php endif; ?>


<?php wp_footer(); ?>
<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>
</html>
