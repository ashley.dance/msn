<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,700" rel="stylesheet">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
	<?php do_action( 'foundationpress_after_body' ); ?>

	<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
	<div class="off-canvas-wrapper">
		<?php get_template_part( 'template-parts/mobile-off-canvas' ); ?>
	<?php endif; ?>

	<?php do_action( 'foundationpress_layout_start' ); ?>

	<header class="site-header" role="banner">
		<div class="row expanded five-padding">

			<div class="column large-6">
				<a href="/"><img width="200" class="logo" src="/wp-content/uploads/2017/08/msn-logo.png" alt="My Social Network Logo"></a>
			</div>

			<div class="column large-6 text-right">
				<a href="#" id="menu-toggle" class="burger"><i class="fa fa-bars" aria-hidden="true"></i></a>
			</div>

		</div>

	</header>

	<div class="fs-menu" id="fs-menu">
		<a href="/"><img width="200" src="/wp-content/uploads/2017/08/msn-logo.png" alt="My Social Network Logo"></a>
		<a href="#" id="menu-close" class="menu-close"><i class="fa fa-times" aria-hidden="true"></i></a>
		
		<div class="menu-content">
			<nav>
				<?php wp_nav_menu( array('menu' => 'Main Menu') ); ?>
			</nav>

			<ul class="header-social">
				<li><a target="_blank" href="<?php the_field('facebook','option'); ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a target="_blank" href="<?php the_field('twitter','option'); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
				<li><a target="_blank" href="<?php the_field('instagram','option'); ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			</ul>

		</div>

	</div>

	<section class="container">
		<?php do_action( 'foundationpress_after_header' );
