$(document).ready(function(){

	// Smooth anchor scrolling
	$('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') || location.hostname == this.hostname) {
	        var target = $(this.hash);
	        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	           if (target.length) {
	             $('html,body').animate({
	                 scrollTop: target.offset().top
	            }, 1000);
	            return false;
	        }
	    }
	});

	// Menu
	$('#menu-toggle, #menu-close').click(function(event){
		event.preventDefault();

		$('#fs-menu').fadeToggle();
	});

	// Fade out menu when clicked
	$('#menu-main-menu a').click(function(event){
		$('#fs-menu').fadeOut();
	});

	// Tiles
	$('.single-tile').click(function(event){
		event.preventDefault();

		var tileColour 	=	$(this).attr('data-colour');
		var tileID 		=	$(this).attr('data-id');

		// Check tile isn't first or last
		if ( tileID == 1 ) {
			$('#tile-prev').addClass('inactive');
		} else if ( tileID == 6 ) {
			$('#tile-next').addClass('inactive');
		}

		$('.full-tile').addClass(tileColour);

		$('.full-tile').fadeIn(function(){
			$('#tile-' + tileID).addClass('active').fadeIn();
		});

	});

	/*
	1 -  Click
	2 - Either add or minus
	*/

	$('#tile-prev, #tile-next').click(function(event){
		event.preventDefault();

		// Setup Variables
		var direction 		=	$(this).attr('id');
		var activeTile		=	$('.tile-content.active');
		var activeID		=	parseInt(activeTile.attr('data-id'));
		var activeColour	=	activeTile.attr('data-colour');

		// Check the direction
		if ( direction == 'tile-next' ) {
			var newTileID = activeID + 1;
			console.log(newTileID);
		} else {
			var newTileID = activeID - 1;
			console.log(newTileID);
		}

		// Disable controls for first and last
		if ( newTileID == 1 ) {
			$('#tile-prev').addClass('inactive');
		} else if ( newTileID == 6 ) {
			$('#tile-next').addClass('inactive');
		} else {
			$('#tile-prev').removeClass('inactive');
			$('#tile-next').removeClass('inactive');
		}

		// Get the new colour
		var newColour = $('#tile-' + newTileID).attr('data-colour');

		// fade out current tile
		activeTile.fadeOut().removeClass('active');

		// hide old tile, display new tile
		$('.full-tile').removeClass(activeColour).addClass(newColour);
		$('.tile-content.active').fadeOut().removeClass('active');
		$('#tile-' + newTileID).fadeIn().addClass('active');

	});

	// Close the tile
	$('#tile-close').click(function(event){
		event.preventDefault();

		$('.full-tile').fadeOut(function(){
			$(this).removeClass().addClass('full-tile');
		});

		$('.tile-content').fadeOut();
		$('.tile-content.active').removeClass('active');

		// Reset tile controls
		$('#tile-prev').removeClass('inactive');
		$('#tile-next').removeClass('inactive');

	});

});