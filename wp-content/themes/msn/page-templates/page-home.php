<?php
/*
Template Name: Home Page
*/
get_header(); ?>

<section class="hero" style="background-image: url('/wp-content/uploads/2017/08/hero-background.jpg')">
	<h1><?php the_title(); ?></h1>
	<a href="#who-we-are"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
</section><!-- .hero -->

<section class="intro" id="who-we-are">
	<?php  while ( have_posts() ) : the_post(); ?>
	<div class="row expanded five-padding align-middle">
		<div class="column large-6 medium-12 small-12">
			<?php the_content(); ?>
		</div>
		<div class="column large-6 medium-12 small-12 text-right">
			<?php the_post_thumbnail(); ?>
		</div>
	</div>
	<?php endwhile; ?>
</section><!-- .intro -->

<section class="tiles" id="what-we-do">
	
	<div class="tile-wrap">
		
		<?php $count = 1; ?>

		<?php while ( have_rows('services') ) : the_row(); ?>

			<div class="single-tile <?php the_sub_field('service_colour'); ?>" data-colour="<?php the_sub_field('service_colour'); ?>" data-id="<?php echo $count; ?>">
				<div class="service-title">
					<?php the_sub_field('service_title'); ?>
				</div>
			</div>

			<?php $count++; ?>

		<?php endwhile; ?>
	</div>

	<div class="full-tile">
		<a href="#" id="tile-close" class="tile-close"><i class="fa fa-times" aria-hidden="true"></i></a>
		
		<?php $count = 1; ?>

		<?php while ( have_rows('services') ) : the_row(); ?>
			<div class="tile-content" id="tile-<?php echo $count; ?>" data-id="<?php echo $count; ?>" data-colour="<?php the_sub_field('service_colour'); ?>">
				<div class="tile-title">
					<?php the_sub_field('service_title'); ?>
				</div>
				<div class="tile-body">
					<?php the_sub_field('service_content'); ?>
				</div>
			</div>
			<?php $count++; ?>
		<?php endwhile; ?>

		<div class="tile-controls">
			<a href="#" id="tile-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
			<a href="#" id="tile-next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
		</div>

	</div>

</section><!-- .tiles -->

<section class="our-clients" id="our-clients">
	<div class="row expanded five-padding">
		<div class="column large-12">
			<h2>Our Clients. </h2>

			<div class="clients">
				<?php while ( have_rows('clients') ) : the_row(); ?>
					<?php $image = get_sub_field('client'); ?>
					<div class="single-client">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
					</div>
				<?php endwhile; ?>
			</div>

		</div>
	</div>
</section><!-- .our-clients -->

<section class="contact" id="get-in-touch">
	<div class="flex-wrap">
		<div class="row expanded five-padding">
			<div class="column large-12">
				<h2>Get in touch.</h2>

				<div class="row expanded large-collapse">
					<div class="column large-5 medium-12 small-12"> 
						<?php echo do_shortcode('[contact-form-7 id="30" title="Contact form 1"]'); ?>
					</div>
					<div class="column large-6 medium-12 small-12 large-offset-1">
						<ul class="contact-details">
							<li class="email"><a href="mailto:<?php the_field('email_address','option'); ?>"><?php the_field('email_address','option'); ?></a></li>
							<li class="phone"><a href="tel:<?php the_field('phone_number','option'); ?>"><?php the_field('phone_number','option'); ?></a></li>
							<li class="address"><?php the_field('address','option'); ?></li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</div>
</section><!-- .contact -->

<?php get_footer();
